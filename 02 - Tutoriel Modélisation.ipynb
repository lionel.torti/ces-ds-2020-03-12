{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from pylab import *\n",
    "import matplotlib.pyplot as plt\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating your first Bayesian Network with pyAgrum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(This example is based on an OpenBayes [closed] website tutorial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p>A <b>Bayesian network</b> (BN) is composed of <b>random variables</b> (nodes) and their conditional dependencies (arcs) which, together, form a directed acyclic graph (DAG). A <b>conditional probability table</b> (CPT) is associated with each node. It contains the conditional probability distribution of the node given its parents in the DAG:\n",
    "<center><img src=\"waterprinkler.png\"></center>\n",
    "Such a BN allows to manipulate the joint probability $P(C,S,R,W)$&nbsp;&nbsp;&nbsp;using this decomposition :\n",
    "<center>\n",
    "    $P(C,S,R,W)=\\prod_X P(X | Parents_X) = P(C) \\cdot P(S | C) \\cdot P(R | C) \\cdot P(W | S,R)$\n",
    "</center>\n",
    "</p>\n",
    "<p>\n",
    "    Imagine you want to create your first Bayesian network, say for example the 'Water Sprinkler' network. This is an easy example. All the nodes are Boolean (only 2 possible values). You can proceed as follows.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import the pyAgrum package"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyAgrum as gum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create the network topology"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create the BN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next line creates an empty BN network with a 'name' property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn=gum.BayesNet('WaterSprinkler')\n",
    "print(bn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create the variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "pyAgrum(aGrUM) provides 3 types of variables :\n",
    "<ul>\n",
    "    <li>LabelizedVariable</li>\n",
    "    <li>RangeVariable</li>\n",
    "    <li>DiscretizedVariable</li>\n",
    "</ul>\n",
    "In this tutorial, we will use LabelizedVariable, which is a variable whose domain is a finite set of labels. The next line will create a variable named 'c', with 2 values and described as 'cloudy?', and it will add it to the BN. The value returned is the id of the node in the graphical structure (the DAG). pyAgrum actually distinguishes the random variable (here the labelizedVariable) from its node in the DAG: the latter is identified through a numeric id. Of course, pyAgrum provides functions to get the id of a node given the corresponding variable and conversely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c=bn.add(gum.LabelizedVariable('c','cloudy ?',2))\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can go on adding nodes in the network this way. Let us use python to compact a little bit the code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s, r, w = [ bn.add(name, 2) for name in \"srw\" ] #bn.add(name, 2) === bn.add(gum.LabelizedVariable(name, name, 2))\n",
    "print (s,r,w)\n",
    "print (bn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create the arcs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have to connect nodes, i.e., to add arcs linking the nodes. Remember that <tt>c</tt> and <tt>s</tt> are ids for nodes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.addArc(c,s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once again, python can help us :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for link in [(c,r),(s,w),(r,w)]:\n",
    "    bn.addArc(*link)\n",
    "print(bn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "pyAgrum provides tools to display <tt>bn</tt> in more user-frendly fashions. <br/>Notably, <tt>pyAgrum.lib</tt> is a set of tools written in pyAgrum to help using aGrUM in python. <tt>pyAgrum.lib.notebook</tt> adds dedicated functions for iPython notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyAgrum.lib.notebook as gnb\n",
    "bn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create the probability tables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the network topology is constructed, we must initialize the conditional probability tables (CPT) distributions.\n",
    "Each CPT is considered as a Potential object in pyAgrum. There are several ways to fill such an object.<br/>\n",
    "\n",
    "To get the CPT of a variable, use the cpt method of your BayesNet instance with the variable's id as parameter.<br/>\n",
    "\n",
    "Now we are ready to fill in the parameters of each node in our network. There are several ways to add these parameters.<br/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Low-level way"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(c).fillWith([0.5,0.5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most of the methods using a node id will also work with name of the random variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(\"c\").fillWith([0.4,0.6])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using the order of variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(s).var_names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(s)[:]=[ [0.5,0.5],[0.9,0.1]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then $P(S | C=0)=[0.5,0.5]$ <br/>and $P(S | C=1)=[0.9,0.1]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(bn.cpt(s)[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same process can be performed in several steps:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(s)[0,:]=0.5 # equivalent to [0.5,0.5]\n",
    "bn.cpt(s)[1,:]=[0.9,0.1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(w).var_names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(w)[0,0,:] = [1, 0] # r=0,s=0\n",
    "bn.cpt(w)[0,1,:] = [0.1, 0.9] # r=0,s=1\n",
    "bn.cpt(w)[1,0,:] = [0.1, 0.9] # r=1,s=0\n",
    "bn.cpt(w)[1,1,:] = [0.01, 0.99] # r=1,s=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using a dictionnary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is probably the most convenient way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(w)[{'r': 0, 's': 0}] = [1, 0]\n",
    "bn.cpt(w)[{'r': 0, 's': 1}] = [0.1, 0.9]\n",
    "bn.cpt(w)[{'r': 1, 's': 0}] = [0.1, 0.9]\n",
    "bn.cpt(w)[{'r': 1, 's': 1}] = [0.01, 0.99]\n",
    "bn.cpt(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The use of dictionaries is a feature borrowed from OpenBayes. It facilitates the use and avoid common errors that happen when introducing data into the wrong places."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn.cpt(r)[{'c':0}]=[0.8,0.2]\n",
    "bn.cpt(r)[{'c':1}]=[0.2,0.8]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input/output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now our BN is complete. It can be saved in different format :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(gum.availableBNExts())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can save a BN using BIF format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gum.saveBN(bn,\"WaterSprinkler.bif\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"WaterSprinkler.bif\",\"r\") as out:\n",
    "    print(out.read())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn2=gum.loadBN(\"WaterSprinkler.bif\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also save and load it in other formats"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gum.saveBN(bn,\"WaterSprinkler.net\")\n",
    "with open(\"WaterSprinkler.net\",\"r\") as out:\n",
    "    print(out.read())\n",
    "bn3=gum.loadBN(\"WaterSprinkler.net\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inference in Bayesian Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have to choose an inference engine to perform calculations for us. Two inference engines are currently available in pyAgrum:\n",
    "<ul>\n",
    "    <li><b>LazyPropagation</b>: an exact inference method that transforms the Bayesian network into a hypergraph called a join tree or a junction tree. This tree is constructed in order to optimize inference computations.</li>\n",
    "    <li><b>Gibbs</b> : an approximate inference engine using the Gibbs sampling algorithm to generate a sequence of samples from the joint probability distribution.</li>\n",
    "</ul>\n",
    "        \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ie=gum.LazyPropagation(bn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inference without evidence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ie.makeInference()\n",
    "print (ie.posterior(w))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "In our BN, $P(W) = [ 0.3529,\\ \\  0.6471]$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With notebooks, it can be viewed as an HTML table"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ie.posterior(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inference with evidence"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose now that you know that the sprinkler is on and that it is not cloudy, and you wonder what Is the probability of the grass being wet, i.e., you are interested in distribution $P(W|S=1,C=0)$. <br/>The new knowledge you have (sprinkler is on and it is not cloudy) is called evidence. Evidence is entered using a dictionary. When you know precisely the value taken by a random variable, the evidence is called a hard evidence. This is the case, for instance, when I know for sure that the sprinkler is on. In this case, the knowledge is entered in the dictionary as 'variable name':label"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ie.setEvidence({'s':0, 'c': 0})\n",
    "ie.makeInference()\n",
    "ie.posterior(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you have incomplete knowledge about the value of a random variable, this is called a soft evidence. In this case, this evidence is entered as the belief you have over the possible values that the random variable can take, in other words, as <em>P(evidence|true value of the variable)</em>. Imagine for instance that you think that if the sprinkler is off, you have only 50% chances of knowing it, but if it is on, you are sure to know it. Then, your belief about the state of the sprinkler is [0.5, 1] and you should enter this knowledge as shown below. Of course, hard evidence are special cases of soft evidence in which the beliefs over all the values of the random variable but one are equal to 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ie.setEvidence({'s': [0.5, 1], 'c': [1, 0]})\n",
    "ie.makeInference()\n",
    "ie.posterior(w) # using gnb's feature"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "the pyAgrum.lib.notebook utility proposes certain functions to graphically show distributions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "gnb.showProba(ie.posterior(w))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb.showPosterior(bn,{'s':1,'c':0},'w')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## inference in the whole Bayes net"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb.showInference(bn,evs={})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### inference with evidence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb.showInference(bn,evs={'s':1,'c':0})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### inference with soft and hard evidence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb.showInference(bn,evs={'s':1,'c':[0.3,0.9]})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### inference with partial targets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb.showInference(bn,evs={'c':[0.3,0.9]},targets={'c','w'})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
